# This file is part of the SLA firmware
# Copyright (C) 2023 Prusa Research a.s. - www.prusa3d.com
# SPDX-License-Identifier: GPL-3.0-or-later

from unittest import TestCase, main


class TestIntial(TestCase):

    def test_initial(self) -> None:
        self.assertEqual(0, 0)

if __name__ == "__main__":
    main()
