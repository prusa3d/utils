# SLA utils
![pipeline](https://gitlab.com/prusa3d/utils/badges/master/pipeline.svg)
![coverage](https://gitlab.com/prusa3d/utils/badges/master/coverage.svg)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)

Shared data types and functions over SLA software.
