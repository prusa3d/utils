# -*- coding: utf-8 -*-
# This file is part of the SLA firmware
# Copyright (C) 2022 Prusa Research a.s. - www.prusa3d.com
# SPDX-License-Identifier: GPL-3.0-or-later

__version__ = "0.0.1"
__date__ = "05 Apr 2023"

__copyright__ = "(c) 2023 Prusa 3D"
__author_name__ = "Marek Mosna"
__author_email__ = "marek.mosna@prusa3d.cz"
__author__ = f"{__author_name__} <{__author_email__}>"
__description__ = "Shared data types and functions over SLA software"

__credits__ = "Marek Mosna"
__url__ = "https://gitlab.com/prusa3d/utils.git"
