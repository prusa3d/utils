# -*- coding: utf-8 -*-
# pylint: disable=C0114
# This file is part of the SLA firmware
# Copyright (C) 2020 Prusa Research a.s. - www.prusa3d.com
# SPDX-License-Identifier: GPL-3.0-or-later

"""Setup of Prusa SLA File Manager"""

import re

import setuptools

METADATA = {}
with open("utils/__init__.py", "r", encoding="utf8") as info:
    METADATA = dict(re.findall(r'__([a-z_]+)__ = "([^"]+)"', info.read()))

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="utils",
    version=METADATA["version"],
    author=METADATA["author_name"],
    author_email=METADATA["author_email"],
    maintainer=METADATA["author_name"],
    maintainer_email=METADATA["author_email"],
    url=METADATA["url"],
    description=METADATA["description"],
    long_description=long_description,
    long_description_content_type="text/markdown",
    license="GNU General Public License v3 or later (GPLv3+)",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)",
        "Operating System :: POSIX :: Linux",
    ],
    python_requires=">=3.6",
)
